package com.example.dott

import android.app.Application
import android.content.res.Resources
import com.example.dott.data.RestaurantListRepository
import com.example.dott.scene.restaurants.RestaurantsMapViewModel
import com.example.dott.scene.restaurants.model.RestaurantUIModel
import com.google.common.truth.Truth
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import org.junit.Test

class RestaurantsMapViewModelTest : BaseTest() {
    private lateinit var viewModel: RestaurantsMapViewModel

    @MockK(relaxed = true)
    private lateinit var resources: Resources

    @MockK
    private lateinit var repository: RestaurantListRepository

    @MockK(relaxed = true)
    private lateinit var application: Application

    override fun setUp() {
        viewModel = RestaurantsMapViewModel(
            application,
            repository
        )
        every { application.resources } returns resources
    }

    @Test
    fun `when viewModel initialised set a value to the restaurantList`() {
        // Given
        coEvery { repository.fetchRestaurantList("0.0,0.0") } returns listOf(RestaurantUIModel())

        // When
        viewModel.fetchRestaurantsList(0.0, 0.0)
        val actual = viewModel.restaurantLists.getOrAwaitValue()

        // Then
        Truth.assertThat(actual).isNotEmpty()
    }
}
