package com.example.dott

import com.example.dott.data.RestaurantListDataSource
import com.example.dott.data.RestaurantListRepository
import com.example.dott.data.mapper.RestaurantUIMapper
import com.example.dott.data.model.Meta
import com.example.dott.data.model.Response
import com.example.dott.data.model.RestaurantResponse
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Test

class RestaurantListRepositoryTest : BaseTest() {

    @MockK(relaxed = true)
    lateinit var dataSource: RestaurantListDataSource

    @MockK(relaxed = true)
    lateinit var mapper: RestaurantUIMapper

    @InjectMockKs
    lateinit var repository: RestaurantListRepository

    private val mockedRestaurantResponse = RestaurantResponse(Meta(), Response(listOf()))

    override fun setUp() {
        coEvery { dataSource.fetchRestaurantList("") } returns mockedRestaurantResponse
    }

    @Test
    fun `when fetchRestaurantList is called, repository should return a data`() = runBlocking {
        // Given

        // When
        repository.fetchRestaurantList("")

        // Then
        coVerify { dataSource.fetchRestaurantList("") }
    }
}
