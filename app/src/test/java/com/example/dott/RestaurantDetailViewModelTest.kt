package com.example.dott

import android.app.Application
import android.content.res.Resources
import com.example.dott.scene.restaurants.detail.RestaurantDetailViewModel
import com.example.dott.scene.restaurants.model.RestaurantUIModel
import com.google.common.truth.Truth
import io.mockk.every
import io.mockk.impl.annotations.MockK
import org.junit.Test

class RestaurantDetailViewModelTest : BaseTest() {
    private lateinit var viewModel: RestaurantDetailViewModel

    @MockK(relaxed = true)
    private lateinit var resources: Resources

    @MockK(relaxed = true)
    private lateinit var application: Application

    override fun setUp() {
        viewModel = RestaurantDetailViewModel(
            application
        )
        every { application.resources } returns resources
    }

    @Test
    fun `when setRestaurant is called set a value to the restaurant`() {
        // Given

        // When
        viewModel.setRestaurant(RestaurantUIModel("11"))
        val actual = viewModel.restaurant.getOrAwaitValue()

        // Then
        Truth.assertThat(actual).isEqualTo(RestaurantUIModel("11"))
    }
}
