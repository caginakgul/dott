package com.example.dott

import com.example.dott.data.RestaurantListApi
import com.example.dott.data.RestaurantListDataSource
import com.example.dott.data.model.Meta
import com.example.dott.data.model.Response
import com.example.dott.data.model.RestaurantResponse
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.Test

class RestaurantListDataSourceTest : BaseTest() {

    @MockK(relaxed = true)
    lateinit var api: RestaurantListApi

    @InjectMockKs
    lateinit var dataSource: RestaurantListDataSource

    private val mockedRestaurantResponse = RestaurantResponse(Meta(), Response(listOf()))

    override fun setUp() {
        coEvery { dataSource.fetchRestaurantList(any()) } returns mockedRestaurantResponse
    }

    @Test
    fun `when fetchRestaurantList is called, should return correct data`() = runBlocking {
        // Given

        // When
        dataSource.fetchRestaurantList("")

        // Then
        coVerify { api.fetchRestaurantList("") }
    }
}
