package com.example.dott.scene.base

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import java.lang.reflect.ParameterizedType

abstract class BaseActivity<VM : BaseViewModel> : AppCompatActivity() {

    @Suppress("UNCHECKED_CAST")
    private val persistentViewModelClass = (javaClass.genericSuperclass as ParameterizedType)
        .actualTypeArguments[0] as Class<VM>

    protected val viewModel: VM by lazy { ViewModelProvider(this).get(persistentViewModelClass) }
}
