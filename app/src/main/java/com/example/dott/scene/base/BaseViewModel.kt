package com.example.dott.scene.base

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

abstract class BaseViewModel(application: Application) : AndroidViewModel(application) {

    protected open fun handleFailure(e: IOException) {
        var errorMessage = ""
        errorMessage = when (e) {
            is HttpException -> {
                e.message()
            }
            is SocketTimeoutException -> {
                "Looks like you have an unstable network at the moment, please try again when network stabilizes."
            }
            is UnknownHostException -> {
                "Please check your connectivity."
            }
            else -> {
                "Something went wrong."
            }
        }
        showToast(errorMessage)
    }

    private fun showToast(errorMessage: String) {
        Toast.makeText(
            getApplication(),
            errorMessage,
            Toast.LENGTH_SHORT
        ).show()
    }
}
