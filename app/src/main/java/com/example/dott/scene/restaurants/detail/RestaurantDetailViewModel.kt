package com.example.dott.scene.restaurants.detail

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.dott.scene.base.BaseViewModel
import com.example.dott.scene.restaurants.model.RestaurantUIModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RestaurantDetailViewModel @Inject constructor(
    app: Application
) : BaseViewModel(app) {

    private val _restaurant = MutableLiveData<RestaurantUIModel>()
    val restaurant: LiveData<RestaurantUIModel>
        get() = _restaurant

    fun setRestaurant(restaurantUIModel: RestaurantUIModel) {
        _restaurant.value = restaurantUIModel
    }
}
