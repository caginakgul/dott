package com.example.dott.scene.base

import androidx.annotation.IdRes
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.navGraphViewModels
import com.example.dott.util.lazyThreadSafetyNone
import java.lang.reflect.ParameterizedType
import com.example.dott.BR

class BaseFragmentDecorator<VM : BaseViewModel, B : ViewDataBinding>(
    val fragment: Fragment,
    val binder: B
) {
    @Suppress("UNCHECKED_CAST")
    val viewModel by lazyThreadSafetyNone {
        val persistentViewModelClass = (fragment.javaClass.genericSuperclass as ParameterizedType)
            .actualTypeArguments[0] as Class<VM>
        return@lazyThreadSafetyNone ViewModelProvider(fragment)
            .get(persistentViewModelClass)
    }

    inline fun <reified VM : ViewModel> activityViewModels(): Lazy<VM> {
        return fragment.activityViewModels()
    }

    inline fun <reified VM : ViewModel> viewModels(): Lazy<VM> {
        return fragment.viewModels()
    }

    inline fun <reified VM : ViewModel> parentViewModels(): Lazy<VM> {
        return fragment.requireParentFragment().viewModels()
    }

    inline fun <reified VM : ViewModel> navGraphViewModels(@IdRes navGraphId: Int): Lazy<VM> {
        return fragment.navGraphViewModels(navGraphId)
    }

    init {
        with(binder) {
            lifecycleOwner = fragment.viewLifecycleOwner
            setVariable(BR.viewModel, viewModel)
        }
    }
}
