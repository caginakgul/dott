package com.example.dott.scene.main

import android.os.Bundle
import com.example.dott.R
import com.example.dott.databinding.ActivityMainBinding
import com.example.dott.scene.base.BaseBindingActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseBindingActivity<MainViewModel, ActivityMainBinding>() {

    override val layoutId get() = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binder.viewModel = viewModel
        binder.lifecycleOwner = this
    }
}
