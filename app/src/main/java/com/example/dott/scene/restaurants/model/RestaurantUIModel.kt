package com.example.dott.scene.restaurants.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class RestaurantUIModel(
    val id: String = "",
    val name: String = "",
    val lat: Double = 0.0,
    val lng: Double = 0.0,
    val distance: String = "",
    val city: String = "",
    val phone: String = ""
) : Parcelable
