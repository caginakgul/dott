package com.example.dott.scene.restaurants.detail

import com.example.dott.R
import com.example.dott.databinding.FragmentRestaurantDetailBinding
import com.example.dott.scene.base.BaseBottomSheetFragment
import com.example.dott.scene.restaurants.model.RestaurantUIModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RestaurantDetailFragment(override val layoutId: Int = R.layout.fragment_restaurant_detail) :
    BaseBottomSheetFragment<RestaurantDetailViewModel, FragmentRestaurantDetailBinding>() {

    override fun initialize() {
        arguments?.get(RESTAURANT_KEY)?.let {
            viewModel.setRestaurant(it as RestaurantUIModel)
        }
    }

    companion object {
        const val RESTAURANT_KEY = "argument_detail"
    }
}
