package com.example.dott.scene.restaurants

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.dott.data.RestaurantListRepository
import com.example.dott.scene.base.BaseViewModel
import com.example.dott.scene.restaurants.model.RestaurantUIModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.io.IOException
import javax.inject.Inject

@HiltViewModel
class RestaurantsMapViewModel @Inject constructor(
    app: Application,
    private val restaurantListRepository: RestaurantListRepository
) : BaseViewModel(app) {

    private val _restaurantLists = MutableLiveData<List<RestaurantUIModel>>()
    val restaurantLists: LiveData<List<RestaurantUIModel>>
        get() = _restaurantLists

    fun fetchRestaurantsList(userLat: Double, userLng: Double) {
        viewModelScope.launch {
            try {
                _restaurantLists.value =
                    restaurantListRepository.fetchRestaurantList(
                        userLat.toString().plus(",").plus(userLng.toString())
                    )
            } catch (e: IOException) {
                handleFailure(e)
            }
        }
    }
}
