package com.example.dott.scene.restaurants

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.provider.Settings
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.navigation.Navigation
import com.example.dott.R
import com.example.dott.databinding.FragmentRestaurantsMapBinding
import com.example.dott.scene.base.BaseFragment
import com.example.dott.scene.restaurants.model.RestaurantUIModel
import com.example.dott.util.isLocationEnabled
import com.example.dott.util.toast
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dagger.hilt.android.AndroidEntryPoint
import pub.devrel.easypermissions.EasyPermissions
import timber.log.Timber

@AndroidEntryPoint
class RestaurantsMapFragment : BaseFragment<RestaurantsMapViewModel, FragmentRestaurantsMapBinding>(
    R.layout.fragment_restaurants_map
), OnMapReadyCallback, EasyPermissions.PermissionCallbacks {

    private lateinit var map: GoogleMap
    private var isDataReady: Boolean = false
    private var isMapReady: Boolean = false
    private lateinit var restaurantList: List<RestaurantUIModel>
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var currentLat: Double = 0.0
    private var currentLng: Double = 0.0

    override fun initialize() {
        super.initialize()
        binder.map.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
        checkPermission()
    }

    override fun observeData() {
        super.observeData()
        viewModel.restaurantLists.observe(viewLifecycleOwner) {
            Timber.i("Data is received to UI")
            restaurantList = it
            isDataReady = true
            checkDataIsReadyToDisplay()
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        Timber.i("Map is ready")
        map = googleMap
        isMapReady = true
        checkDataIsReadyToDisplay()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binder) {
            map.onCreate(savedInstanceState)
            map.onResume()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        Timber.i("Location permission is granted.")
        checkLocationIsEnabled()
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        Timber.w("Location permission is denied.")
        requireContext().toast(getString(R.string.location_permission))
    }

    private fun checkLocationIsEnabled() {
        if (!requireContext().isLocationEnabled()) {
            askUserToEnableLocation()
        } else {
            getLastKnownLocation()
        }
    }

    private fun checkPermission() {
        if (hasLocationPermission()) {
            Timber.i("Location permission is already granted.")
            checkLocationIsEnabled()
        } else {
            requestLocationPermission()
        }
    }

    private fun hasLocationPermission() = EasyPermissions.hasPermissions(
        requireContext(),
        Manifest.permission.ACCESS_FINE_LOCATION
    )

    private fun requestLocationPermission() {
        EasyPermissions.requestPermissions(
            this,
            getString(R.string.location_permission),
            PERMISSION_LOCATION_REQUEST_CODE,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    }

    @SuppressLint("MissingPermission")
    private fun getLastKnownLocation() {
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                location?.let {
                    currentLat = location.latitude
                    currentLng = location.longitude
                    viewModel.fetchRestaurantsList(location.latitude, location.longitude)
                    map.isMyLocationEnabled = true
                } ?: Timber.w("Last location is null.")
            }
    }

    private fun askUserToEnableLocation() {
        Timber.w("Location services are disabled.")
        val builder = AlertDialog.Builder(requireContext())
        builder.setMessage(R.string.location_disabled_message)

        builder.setPositiveButton(getString(R.string.positive_button)) { _, _ ->
            startLocationIntent()
        }
        builder.setNegativeButton(getString(R.string.negative_button)) { _, _ ->
            requireContext().toast(getString(R.string.location_disabled_message))
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    /**
     * For some reason, Android system returns RESULT_CANCELLED, even if user successfully turn on location
     * Therefore the if statement (to check result code is OK) is removed for now.
     * The same way checkLocationSettings task also returns resolution required error. Need the investigate.
     */
    private val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            getLastKnownLocation()
        }

    private fun startLocationIntent() {
        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        startForResult.launch(intent)
    }

    private fun checkDataIsReadyToDisplay() {
        if (isDataReady && isMapReady) {
            setMarkers()
        }
    }

    private fun setMarkers() {
        Timber.i("Markers are setting")
        var isReasonGesture = false
        for (restaurant in restaurantList) {
            val restaurantLocation = LatLng(restaurant.lat, restaurant.lng)
            val marker = map.addMarker(
                MarkerOptions()
                    .position(restaurantLocation).title(restaurant.name)
            )
            marker?.tag = restaurant.id
        }
        if (!isReasonGesture) {
            map.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(
                        currentLat, currentLng
                    ), ZOOM_LEVEL
                )
            )
        }

        map.setOnMarkerClickListener { marker ->
            val selectedRestaurant = restaurantList.find { it.id == marker.tag }
            navigateToDetail(selectedRestaurant)
            true
        }

        map.setOnCameraMoveStartedListener {
            if (it == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
                isReasonGesture = true
            }
        }

        map.setOnCameraIdleListener {
            if (isReasonGesture) {
                currentLat = map.cameraPosition.target.latitude
                currentLng = map.cameraPosition.target.longitude
                viewModel.fetchRestaurantsList(currentLat, currentLng)
            }
        }
    }

    private fun navigateToDetail(restaurantUIModel: RestaurantUIModel?) {
        restaurantUIModel?.let {
            val action =
                RestaurantsMapFragmentDirections
                    .actionRestaurantsMapFragmentToRestaurantDetailFragment(
                        it
                    )
            Navigation.findNavController(binder.map).navigate(action)
        }
    }

    companion object {
        const val PERMISSION_LOCATION_REQUEST_CODE = 1
        private const val ZOOM_LEVEL = 16f
    }
}
