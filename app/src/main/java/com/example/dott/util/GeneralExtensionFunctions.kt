package com.example.dott.util

import android.content.Context
import android.location.LocationManager
import android.widget.Toast

fun <T> lazyThreadSafetyNone(initializer: () -> T):
        Lazy<T> = lazy(LazyThreadSafetyMode.NONE, initializer)

fun Context.toast(msg: String, duration: Int = Toast.LENGTH_LONG) {
    Toast.makeText(this, msg, duration).show()
}

fun Context.isLocationEnabled(): Boolean =
    getSystemService(Context.LOCATION_SERVICE)
        .safeCast<LocationManager>()?.isProviderEnabled(LocationManager.GPS_PROVIDER) ?: false

inline fun <reified T : Any> Any?.safeCast(): T? = this as? T
