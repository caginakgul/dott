package com.example.dott.data

import com.example.dott.data.mapper.RestaurantUIMapper
import com.example.dott.scene.restaurants.model.RestaurantUIModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RestaurantListRepository @Inject constructor(
    private val mapper: RestaurantUIMapper,
    private val restaurantListDataSource: RestaurantListDataSource
) {

    suspend fun fetchRestaurantList(latLng: String): List<RestaurantUIModel> {
        val data = restaurantListDataSource.fetchRestaurantList(latLng)
        return data.response.venues.map { mapper.toUiModel(it) }
    }
}
