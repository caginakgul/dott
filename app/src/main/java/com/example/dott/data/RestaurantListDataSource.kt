package com.example.dott.data

import com.example.dott.data.model.RestaurantResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RestaurantListDataSource @Inject constructor(
    private val api: RestaurantListApi,
) {
    suspend fun fetchRestaurantList(latLng: String): RestaurantResponse {
        return api.fetchRestaurantList(latLng)
    }
}
