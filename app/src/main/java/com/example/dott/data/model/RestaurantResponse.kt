package com.example.dott.data.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
data class RestaurantResponse(
    @Json(name = "meta") val meta: Meta,
    @Json(name = "response") val response: Response
) : Parcelable
