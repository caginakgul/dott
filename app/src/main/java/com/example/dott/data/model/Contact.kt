package com.example.dott.data.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
data class Contact(
    @Json(name = "phone") val phone: String?
) : Parcelable
