package com.example.dott.data

import com.example.dott.BuildConfig
import com.example.dott.data.model.RestaurantResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface RestaurantListApi {

    @GET(RESTAURANTS)
    suspend fun fetchRestaurantList(
        @Query(LOCATION_FILTER) latLng: String,
        @Query(CLIENT_ID) clientId: String = BuildConfig.CLIENT_ID,
        @Query(CLIENT_SECRET) clientSecret: String = BuildConfig.CLIENT_SECRET,
        @Query(VERSION) version: Int = VERSION_VALUE,
        @Query(CATEGORY_ID) categoryId: String = CATEGORY_VALUE,
        @Query(RADIUS) radius: Int = RADIUS_VALUE
    ): RestaurantResponse

    companion object {
        const val RESTAURANTS = "v2/venues/search"
        const val LOCATION_FILTER = "ll"
        const val CLIENT_ID = "client_id"
        const val CLIENT_SECRET = "client_secret"
        const val VERSION = "v"
        const val VERSION_VALUE = 20210101
        const val CATEGORY_ID = "categoryId"
        const val CATEGORY_VALUE = "4d4b7105d754a06374d81259"
        const val RADIUS = "radius"
        const val RADIUS_VALUE = 250
    }
}
