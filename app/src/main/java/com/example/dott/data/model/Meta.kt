package com.example.dott.data.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
data class Meta(
    @Json(name = "code") val code: Int = 0,
    @Json(name = "requestId") val requestId: String = ""
) : Parcelable
