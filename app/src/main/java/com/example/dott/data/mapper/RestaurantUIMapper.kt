package com.example.dott.data.mapper

import com.example.dott.data.model.Venue
import com.example.dott.scene.restaurants.model.RestaurantUIModel
import javax.inject.Inject

class RestaurantUIMapper @Inject constructor() {
    fun toUiModel(responseModel: Venue) = with(responseModel) {
        RestaurantUIModel(
            id = this.id,
            name = this.name,
            lat = this.location.lat,
            lng = this.location.lng,
            distance = this.location.distance.toString().plus(" ")
                .plus("meters"),
            city = this.location.city ?: "-",
            phone = this.contact?.phone ?: "-"
        )
    }
}
